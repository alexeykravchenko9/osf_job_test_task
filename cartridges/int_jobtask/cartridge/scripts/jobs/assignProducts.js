"use strict";

var Status = require("dw/system/Status");
var Logger = require("dw/system/Logger");
var CatalogMgr = require("dw/catalog/CatalogMgr");

// IMPEX path of the directory where the XML file will be placed
var TEMP_FILE_DIRECTORY = '/IMPEX/src/jobtask_ak';

/**
 * Job function that creates an XML file of assigned products by selected Brand to the
 * specific category
 * @param {Object} args - Job arguments object
 * @returns {dw/system/Status} - Status of the result job
 */
function execute(args) {
    var brandID = args.BrandID;
    var categoryID = args.CategoryID;
    var isSearchIndexedProducts = args.SearchIndexedProducts;
    var category = CatalogMgr.getCategory(categoryID);
    if (!category) {
        return new Status(
            Status.ERROR,
            'ERROR',
            "Category doesn't exits based on provided ID, please use ID of existing category in the site catalog."
        );
    }

    var productsIDs = getProductsByBrandID(brandID, isSearchIndexedProducts);
    if (productsIDs.length < 1) {
        return new Status(
            Status.OK,
            "Products with selected brand weren't found."
        );
    }

    var storefrontCatalogID = CatalogMgr.getSiteCatalog();
    var result = generateXMLFile(productsIDs, categoryID, storefrontCatalogID.ID);

    if (!result) {
        return new Status(Status.ERROR, "Error on XML file generation");
    }

    return new Status(Status.OK, "OK");
}

/**
 * Get Products IDs by Brand ID
 * @param {string} brandID - Refinement brand ID
 * @param {boolean} isIndexedProducts - Type of the search products
 * @returns {Array<string>} - Array with searched productIDs or empty Array
 */
function getProductsByBrandID(brandID, isIndexedProducts) {
    if (isIndexedProducts) {
        return searchIndexedProductsOnly(brandID);
    }

    return searchAllProducts(brandID);
}


/**
 * Performs search of all products in the catalog by Brand refinement and retrieves only with the selected brand ID
 * @param {string} brandID - Refinement brand ID
 * @returns {Array} - Array with productIDs
 */
function searchAllProducts(brandID) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var productsAll = [];
    var productsIterator = ProductMgr.queryAllSiteProducts();

    if (!productsIterator.hasNext()) {
        return productsAll;
    }

    while (productsIterator.hasNext()) {
        var product = productsIterator.next();
        if (product.brand === brandID) {
            productsAll.push(product.ID);
        }
    }
    productsIterator.close();

    return productsAll;
}

/**
 * Performs search indexed products by Brand refinement and retrivies only with passed brand
 * @param {string} brandID - Refinement brand ID
 * @returns {Array} - Array with productIDs
 */
function searchIndexedProductsOnly(brandID) {
    var ProductSearchModel = require("dw/catalog/ProductSearchModel");

    var productsAll = [];
    var apiProductSearch = new ProductSearchModel();
    apiProductSearch.setRefinementValues("brand", brandID);
    apiProductSearch.search();

    var productsIter = apiProductSearch.products;
    while (productsIter && productsIter.hasNext()) {
        var product = productsIter.next();
        productsAll.push(product.ID);
    }

    return productsAll;
}

/**
 * Performs search of all products in the catalog by Brand refinement and retrieves only with the selected brand ID
 * @param {Array<string>} productIDs - Product IDs
 * @param {string} categoryID - Category ID where products going to be assigned
 * @param {string} storefrontCatalogID - Site catalog ID
 * @returns {boolean} - Result of creation XML file
 */
function generateXMLFile(productIDs, categoryID, storefrontCatalogID) {
    var result = false;
    var File = require('dw/io/File');
    var FileWriter = require('dw/io/FileWriter');
    var XMLStreamWriter = require('dw/io/XMLStreamWriter');
    var srcFolder = new File(TEMP_FILE_DIRECTORY + '/src');
    if (srcFolder.exists()) {
        var srcFiles = srcFolder.listFiles().toArray();
        if (srcFiles.length > 0) {
            srcFiles.forEach(function (file) {
                file.remove();
            });
        }
    }
    srcFolder.mkdirs();

    var srcXMLFile = srcFolder.getFullPath() + '/category_assigment_' + Date.now() + '.xml';
    var xmlFile = new File(srcXMLFile);
    var fileWriter = new FileWriter(xmlFile);
    var xsw = new XMLStreamWriter(fileWriter);
    Logger.info('Start proccessing generation XML file...');

    try {
        xsw.writeStartDocument('UTF-8', '1.0'); /* eslint-disable indent */
        xsw.writeStartElement("catalog");
        xsw.writeAttribute("xmlns", "http://www.demandware.com/xml/impex/catalog/2006-10-31");
        xsw.writeAttribute("catalog-id", storefrontCatalogID);

        productIDs.forEach(function (pid) {
            xsw.writeStartElement("category-assignment");
            xsw.writeAttribute('category-id', categoryID);
            xsw.writeAttribute('product-id', pid);
            xsw.writeStartElement("primary-flag");
            xsw.writeCharacters("false");
            xsw.writeEndElement(); // end primary-flag
            xsw.writeEndElement(); // end category-assignment
        });

        xsw.writeEndElement(); // catalog
        xsw.writeEndDocument();
        result = true;
        Logger.info('The XML file successfully created...');
    } catch (error) {
        Logger.error('Error while creating XML file: {0}', error);
    } finally {
        xsw.flush();
        xsw.close();
        fileWriter.close();
    }

    return result;
}

module.exports.execute = execute;
